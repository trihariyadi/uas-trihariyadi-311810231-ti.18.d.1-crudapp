# Project UAS Matkul Pemrograman Mobile
## Applikasi Scan QRcode dan Input Data Produksi

Applikasi Scan QRcode dan Input Data Produksi adalah aplikasi CRUD untuk input data produksi baik input secara manual ataupun menggunakan QR Code Scann, aplikasi ini menggunakan penyimpanan database internal yaitu SQLite database.
<br/>

Untuk Clone Repository :
```sh
git clone https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp
```

## Deployment
- Download Android Studio 4.1.2
- Download SDK
- Install SDK terbaru API 30
- Pilih Import Project
- Pilih Directori dimana anda clone Project
- Download beberapa file yang dibutuhkan, Android Studio akan memberikan notifikasi jika membutuhkan beberapa tambahan data
- Run aplikasi dengan Android Studio ke Emulator yang ada, minimal SDK API 21 Android 5.0 (Lollipop)

## Berikut hasil running Applikasi Scan QRcode dan Input Data Produksi di device android:

- [Splash Screen]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/splashScreen.jpeg" width="250"> <br/>

- [Get Started]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/getStarted.jpeg" width="250"> <br/>

- [Sign Up]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/SignUp.jpeg" width="250"> <br/>

- [Login Page]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/Login.jpeg" width="250"> <br/>

- [Home Page (List daily production)]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/ListProd.jpeg" width="250"> <br/>

- [ADD Parameter Page]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/addParameter.jpeg" width="250"> <br/>

- [Profile Page]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/profile.jpeg" width="250"> <br/>

- [ADD Material Page]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/addMaterial.jpeg" width="250"> <br/>

- [ADD Sensor Page]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/addSensor.jpeg" width="250"> <br/>

- [Add Daily Production By Scann QR Code Or Manual input in Floating Action Button]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/AddProd.jpeg" width="250"> <br/>

- [ADD Daily Production Page]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/AddProdField.jpeg" width="250"> <br/>

- [Update Data Daily Production]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/updateProd.jpeg" width="250"> <br/>

- [Detail Production Page]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/detailProd.jpeg" width="250"> <br/>

- [Confirm Delete data Daily Production]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/deleteConfirm.jpeg" width="250"> <br/>

- [Scann QR Code to add data Daily Production]<br/><br/>
&nbsp;&nbsp;<img src="https://gitlab.com/trihariyadi/uas-trihariyadi-311810231-ti.18.d.1-crudapp/-/raw/master/imageDemo/ScanQRCode.jpeg" width="250"> <br/>











































