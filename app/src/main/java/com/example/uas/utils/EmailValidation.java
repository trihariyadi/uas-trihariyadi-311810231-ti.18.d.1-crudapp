package com.example.uas.utils;

import android.text.Editable;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Code BY TRI HARIYADI
 * Technical Information
 * Pelita Bangsa University
 * Class : TI.18.D1
 * NIM : 311810231
 */

public class EmailValidation {
    public boolean isValidEmail(Editable email) {
        String expression = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email.toString());
        return matcher.matches();
    }
}
